module.exports = function (grunt, options) {

    return {

        css: {

            options: {
                livereload: true
            },

            files: ["src/sass/**/*.scss"],
            tasks: ["sass:dev"]
        },

        js: {

            options: {
                livereload: true
            },

            files: ["src/js/*.js"],
            tasks: ["uglify:app-dev"]
        },

        html: {

            options: {
                livereload: true
            },

            files: ["src/**/*.html"],
            tasks: ["processhtml:dev"]
        },

        img: {

            options: {
                livereload: true
            },

            files: ["src/images/**/*"],
            tasks: ["copy:img"]
        },

        fonts: {

            options: {
                livereload: true
            },

            files: ["src/fonts/**/*"],
            tasks: ["copy:fonts"]
        }

    };
};
