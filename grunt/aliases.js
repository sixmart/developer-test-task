module.exports = function (grunt, options) {

    return {

        'default': [

            "clean",
            "sass:dev",
            "copy",
            "processhtml:dev",
            "uglify:app-dev",
            "uglify:vendor",
            "connect",
            "watch"
        ],

        'build': [

            "clean",
            "sass:build",
            "copy",
            "processhtml:build",
            "postcss:build",
            "uglify:app-build",
            "uglify:vendor"
        ]

    };
};