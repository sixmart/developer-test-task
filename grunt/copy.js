module.exports = function (grunt, options) {

    return {

        img: {
            files: [
                {expand: true, cwd: "src/images", src: ["**"], dest: "build/images"}
            ]
        },

        fonts: {
            files: [
                {expand: true, cwd: "src/fonts", src: ["**"], dest: "build/fonts"}
            ]
        }
    };
};
