module.exports = function (grunt, options) {

    return {

        server: {
            options: {
                port: 8080,
                hostname: "*",
                base: "build"
            }
        }
    };
};