module.exports = function (grunt, options) {

    return {

        options: {
            mangle: false,
            sourceMap: false
        },

        vendor: {

            files: {
                'build/js/vendor.min.js': [
                    'src/js/vendor//modernizr.custom.js',
                    'src/js/vendor/classie.js',
                    'src/js/vendor/mlpushmenu.js',
                ]
            }
        },

        'app-dev': {

            options: {
                sourceMap: true
            },

            files: {
                'build/js/app.js': [
                    'src/js/main.js'
                ]
            }
        },

        'app-build': {

            files: {
                'build/js/app.min.js': [
                    'src/js/main.js'
                ]
            }
        }
    };
};