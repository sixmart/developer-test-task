module.exports = function (grunt, options) {

    return {

        options: {
            process: true
        },

        dev: {
            files: {
                "build/index.html": ['src/index.html']
            }
        },

        build: {
            files: {
                "build/index.html": ['src/index.html']
            }
        }
    };
};
