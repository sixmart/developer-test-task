module.exports = function (grunt, options) {

    return {
        /*options: {
            paths: ["src/sass"]
        },*/

        dev: {

            options: {
                sourcemap: 'auto',
                style: 'expanded'
            },

            files: {
                "build/styles/styles.css": "src/sass/main.scss"
            }
        },

        build: {

            options: {
                sourcemap: 'none'
            },

            files: {
                "build/styles/styles.min.css": "src/sass/main.scss"
            }
        }
    };
};