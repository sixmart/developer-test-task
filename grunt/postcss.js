module.exports = function (grunt, options) {

    return {

        options: {

            map: false,

            processors: [
                require("autoprefixer")({browsers: 'last 4 versions'}),
                require('cssnano')()
            ]
        },
        build: {
            src: "build/styles/*.css"
        }
    };
};
