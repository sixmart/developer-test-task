# Dev Test Task project setup

## Project dependencies

* Node.js
* Grunt
* Yarn - https://yarnpkg.com/lang/en/docs/install/
* GIT - global

## Project configuration/setup/install

To install this project run ``yarn`` in CMD

To run this project locally run ``grunt`` in CMD

To compile a static build run ``grunt build`` in CMD