/***
 * Grunt Task to automate development and deployment
 **/
module.exports = function (grunt) {

    grunt.file.defaultEncoding = 'utf-8';

    require('load-grunt-config')(grunt, {});
};